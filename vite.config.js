import { sveltekit } from '@sveltejs/kit/vite';
//https://stackoverflow.com/questions/72449371/jsconfig-path-alias-is-not-working-sveltekit
import path from 'path';

const config = {
	resolve: {
		alias: {
			$lib: path.resolve('./src/lib/'),
			$models: path.resolve('./src/models'),
			$components: path.resolve('./src/components'),
			$api: path.resolve('./src/routes/api'),
			$assets: path.resolve('./src/assets'),
			$routes: path.resolve('./src/routes')
		}
	},
	plugins: [sveltekit()]
};

export default config;
