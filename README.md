## Feb 03, 2023

- npm install ethereum/web3.js (instead of npm install web3?)
- jang js (https://www.youtube.com/watch?v=kGzrb1EY_AA)
- https://youtu.be/gyMwXuJrbJQ (32 hour free code camp web3)

## Feb 02, 2023

- https://www.reddit.com/r/sveltejs/comments/wz38or/how_do_i_properly_wait_for_component_mounted_and/
- pwede ba to? (https://svelte.dev/repl/4aa803de3a56408a97749a7d81a46b3b?version=3.42.4)
- https://www.youtube.com/watch?v=b41OPU0-4bY

## Feb 01, 2023

- install sveltekit (https://kit.svelte.dev/)
- install tailwind for svelte(https://tailwindcss.com/docs/guides/sveltekit)

# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm create svelte@latest

# create a new project in my-app
npm create svelte@latest my-app
```

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
