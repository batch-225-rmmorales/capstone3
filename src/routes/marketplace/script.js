import { json } from '@sveltejs/kit';
import { onMount } from 'svelte';
import { apiData } from './store';

onMount(async () => {
	async function getMarketplace() {
		const response = await fetch('https://dummyjson.com/products?limit=100');
		const data = await response.json();
		console.log(data);
		apiData.set(data);
	}
	await getMarketplace();
});
